<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'edit'),
    'type' => 'file')); ?>

<?php echo $this->Form->input('username', ['placeholder' => 'username', 'class' => 'form-control', 'id' => 'inputUsername', 'autofocus' => true]) ?>

<?php echo $this->Form->input('password', ['placeholder' => 'password', 'type' => 'password', 'class' => 'form-control', 'id' => 'inputPass', 'autofocus' => true]) ?>

<?php echo $this->Form->input('email', ['placeholder' => 'email', 'type' => 'email', 'class' => 'form-control', 'id' => 'inputDescription', 'autofocus' => true]) ?>

<?php echo $this->Form->input('address', ['placeholder' => 'address', 'class' => 'form-control', 'id' => 'inputPrice', 'autofocus' => true]) ?>

    <div class="form-group">
        <label for="gender">Gender</label>
        <div class="radio">
            <label>
                <input type="radio" name="data[User][gender]" id="gender" value="1" checked>
                Nam
            </label>
            <label>
                <input type="radio" name="data[User][gender]" value="0">
                Nữ
            </label>

        </div>
    </div>
<?php echo $this->Form->input('phone', ['placeholder' => 'phone', 'class' => 'form-control']) ?>

<?php echo $this->Form->input('img', ['placeholder' => 'image', 'class' => 'form-control',
    'type' => 'file', ' name' => 'data[users][img]', 'ACCEPT' => 'image/*', 'autofocus' => true]) ?>

<?php echo $this->Form->input('dateofbirth', ['placeholder' => 'dateofbirth', 'class' => 'form-control']) ?>

<?php echo $this->Form->input('role_id', ['placeholder' => 'role_id', 'class' => 'form-control', 'id' => 'inputRoleId', 'autofocus' => true]) ?>
    <br/>
    <p><button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" onclick="history.go(-1)" class="btn btn-default">Back</button></p>

<?php echo $this->Form->end(); ?><?php
