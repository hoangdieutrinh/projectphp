<div class="container">
    <table class="table" border='1' cellpadding='5'>
        <thead>
        <tr>

            <th>username</th>
            <th>email</th>
            <th>address</th>
            <th>phone</th>
            <th>gender</th>
            <th>dateofbirth</th>
            <th>create_time</th>
            <th>role_id</th>
            <th>image</th>


        </tr>
        </thead>

        <tbody>
        <?php foreach ($data as $ix => $row): ?>
            <tr>

                <td><?php echo $row['User']['username']; ?></td>
                <td><?php echo $row['User']['email']; ?></td>
                <td><?php echo $row['User']['address']; ?></td>
                <td><?php echo $row['User']['phone']; ?></td>
                <td><?php echo $row['User']['gender']; ?></td>

                <td><?php echo $row['User']['dateofbirth']; ?></td>
                <td><?php echo $row['User']['create_time']; ?></td>

                <td><?php echo $row['User']['role_id']; ?></td>
                <td>
                    <img src="<?php echo $this->webroot .'/'. 'files' . '/' . $row['User']['username']. '/' . 'avatar' . '/'
                       . $row['User']['img'];?>" alt="" width="200" height="auto">
                </td>

                <td>
                    <ul>
                        <li><?php echo $this->Html->link
                            ('Edit', array('action' => 'edit', $row['User']['id'])); ?> </li>
                        <li><?php echo $this->Form->postLink
                            ('Delete', array('action' => 'delete', $row['User']['id']
                            )); ?> </li>
                        <li><?php echo $this->Html->link
                            ('List', array('action' => 'index')); ?> </li>
                        <li><?php echo $this->Html->link
                            ('New', array('action' => 'add')); ?> </li>
                    </ul>
                </td>
            </tr>


        <?php endforeach ?>
        </tbody>
    </table>
</div>