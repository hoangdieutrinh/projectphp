<?php echo $this->Form->create('News', array('url' => array('controller' => 'news', 'action' => 'add'),
    'type' => 'file')); ?>

<?php echo $this->Form->input('category_id', ['placeholder' => 'category_id', 'class' => 'form-control',]) ?>

<?php echo $this->Form->input('user_id', ['placeholder' => 'user_id', 'class' => 'form-control',]) ?>

<?php echo $this->Form->input('name', ['placeholder' => 'name', 'class' => 'form-control', ]) ?>

<?php echo $this->Form->input('slug', ['placeholder' => 'slug', 'class' => 'form-control',]) ?>

<?php echo $this->Form->input('description', ['placeholder' => 'description', 'class' => 'form-control',]) ?>

<?php echo $this->Form->input('content', ['placeholder' => 'content', 'class' => 'form-control', ]) ?>

<?php echo $this->Form->input('img', ['placeholder' => 'image', 'class' => 'form-control', 'id' => 'inputImage', 'type' => 'file', ' name' => 'data[news][img]', 'ACCEPT' => 'image/*', 'autofocus' => true]) ?>

<button type="submit" class="btn btn-primary">Submit</button>
<?php echo $this->Form->end(); ?>
