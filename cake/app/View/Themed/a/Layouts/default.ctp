<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $this->fetch('title'); ?></title>

    <?php
        echo $this->Html->css([
            'bootstrap.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
            'AdminLTE.min.css',
            // AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load.
            '_all-skins.min.css',
            // Custom style
            'style.css',
        ]);
    ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <?php echo $this->fetch('css'); ?>
</head>
<body class="skin-blue">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <?php echo $this->element('header');?>
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li><a href="http://almsaeedstudio.com">Web Bán Hàng</a></li>
                        <li><a href="http://almsaeedstudio.com/premium">Admin</a></li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">DANH SÁCH QUẢN LÝ</li>
                    <li class="active"><a href="#"><i class="fa fa-circle-o"></i> Quản Lý Sản Phẩm</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Quản Lý Thư Mục</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Quản Lý Người Dùng</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Tìm Kiếm</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i>Thanh Toán </a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Quản Lý TinTức</a></li>
                    <li class="treeview" id="scrollspy-components">
                        <a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Quản Lý Đơn Hàng</a>
                        <!--
                        <ul class="nav treeview-menu">
                            <li><a href="#">Main Header</a></li>
                            <li><a href="#">Sidebar</a></li>
                            <li><a href="#">Control Sidebar</a></li>
                            <li><a href="#">Info Box</a></li>
                            <li><a href="#">Boxes</a></li>
                            <li><a href="#">Direct Chat</a></li>
                        </ul>
                        -->
                    </li>
                    <!--
                    <li><a href="#plugins"><i class="fa fa-circle-o"></i> Plugins</a></li>
                    <li><a href="#browsers"><i class="fa fa-circle-o"></i> Browser Support</a></li>
                    <li><a href="#upgrade"><i class="fa fa-circle-o"></i> Upgrade Guide</a></li>
                    <li><a href="#implementations"><i class="fa fa-circle-o"></i> Implementations</a></li>
                    <li><a href="#faq"><i class="fa fa-circle-o"></i> FAQ</a></li>
                    <li><a href="#license"><i class="fa fa-circle-o"></i> License</a></li>
                    -->
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>
                <?php echo $this->element('sql_dump'); ?>
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <?php echo $this->element('footer');?>
    </div><!-- ./wrapper -->

    <?php
        echo $this->Html->script([
            'jquery-2.2.3.min.js',
            'bootstrap.min.js',
            'fastclick.min.js',
            'jquery.slimscroll.min.js',
            // // AdminLTE App
            'app.min.js',
        ]);
    ?>

    <?php echo $this->fetch('script'); ?>
</body>
</html>