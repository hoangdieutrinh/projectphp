<div class="sidebar" id="scrollspy">
    <aside class="main-sidebar">

    <ul class="nav sidebar-menu">
        <li class="header">TABLE OF CONTENTS</li>
        <li class="active"><a href="#introduction"><i class="fa fa-circle-o"></i> Introduction</a></li>
        <li><a href="#download"><i class="fa fa-circle-o"></i> Download</a></li>
        <li><a href="#dependencies"><i class="fa fa-circle-o"></i> Dependencies</a></li>
        <li><a href="#advice"><i class="fa fa-circle-o"></i> Advice</a></li>
        <li><a href="#layout"><i class="fa fa-circle-o"></i> Layout</a></li>
        <li><a href="#adminlte-options"><i class="fa fa-circle-o"></i> Javascript Options</a></li>
        <li class="treeview" id="scrollspy-components">
            <a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Components</a>
            <ul class="nav treeview-menu">
                <li><a href="#component-main-header">Main Header</a></li>
                <li><a href="#component-sidebar">Sidebar</a></li>
                <li><a href="#component-control-sidebar">Control Sidebar</a></li>
                <li><a href="#component-info-box">Info Box</a></li>
                <li><a href="#component-box">Boxes</a></li>
                <li><a href="#component-direct-chat">Direct Chat</a></li>
            </ul>
        </li>
        <li><a href="#plugins"><i class="fa fa-circle-o"></i> Plugins</a></li>
        <li><a href="#browsers"><i class="fa fa-circle-o"></i> Browser Support</a></li>
        <li><a href="#upgrade"><i class="fa fa-circle-o"></i> Upgrade Guide</a></li>
        <li><a href="#implementations"><i class="fa fa-circle-o"></i> Implementations</a></li>
        <li><a href="#faq"><i class="fa fa-circle-o"></i> FAQ</a></li>
        <li><a href="#license"><i class="fa fa-circle-o"></i> License</a></li>
    </ul>
</div>
<!-- /.sidebar -->
</aside>
