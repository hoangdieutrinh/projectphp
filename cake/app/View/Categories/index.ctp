<div class="container">
    <table class="table" cellpadding="1">
        <thead>
        <th>id</th>
        <th>name</th>
        <th>description</th>
        <th>slug</th>
        <th>create_time</th>
        <th>Action</th>
        </thead>
        <tbody>
       <!-- --><?php /*pr($data);*/?>
        <?php foreach ($data as $row):?>
        <tr>
            <td><?php echo$row['Category']['id'];  ?></td>
            <td><?php echo $row['Category']['name'] ;?></td>
            <td><?php echo $row['Category']['description']?></td>
            <td> <?php echo $row['Category']['slug'] ;?></td>
            <td><?php echo $row['Category']['create_time']?></td>
            <td>
                <ul>
                    <li><?php echo $this->Html->link
                        ('Edit', array('action' => 'edit', $row['Category']['id'])); ?> </li>
                    <li><?php echo $this->Form->postlink
                        ('Delete',array('action'=>'delete', $row['Category']['id']));?> </li>
                    <li><?php echo $this->Html->link
                        ('List', array('action' => 'index')); ?> </li>
                    <li><?php echo $this->Html->link
                        ('New', array('action' => 'add')); ?> </li>
                </ul>
            </td>
        </tr>
        <?php endforeach;?>

        </tbody>

    </table>
</div>