<?php echo $this->Form->create('Product',array(
    'url'=>array('controller'=>'Products','action'=>'edit')));?>


<?php echo $this->Form->input('category_id', ['placeholder' => 'category_id', 'class' =>'form-control','id' =>'inputCategoryId', 'autofocus'=>true])?>
<?php echo $this->Form->input('status_id', ['placeholder' => 'status_id', 'class' =>'form-control','id' =>'inputStatusId', 'autofocus'=>true])?>

<?php echo $this->Form->input('name', ['placeholder' => 'name', 'class' =>'form-control','id' =>'inputUsername', 'autofocus'=>true])?>

<?php echo $this->Form->input('slug', ['placeholder' => 'slug', 'class' =>'form-control','id' =>'inputSlug', 'autofocus'=>true])?>

<?php echo $this->Form->input('description', ['placeholder' => 'description', 'class' =>'form-control','id' =>'inputDescription','autofocus'=>true])?>

<?php echo $this->Form->input('price', ['placeholder' => 'price', 'class' =>'form-control','id' =>'inputPrice', 'autofocus'=>true])?>

<?php echo $this->Form->input('content', ['placeholder' => 'content', 'class' =>'form-control','id' =>'inputContent', 'autofocus'=>true])?>

<?php echo $this->Form->input('img', ['placeholder' => 'image', 'class' => 'form-control', 'id' => 'inputImage', 'type' => 'file', ' name' => 'data[products][img]', 'ACCEPT' => 'image/*' ]) ?>
<?php echo $this->Form->input('quantity', ['placeholder' => 'số lượng', 'class' =>'form-control','id' =>'inputQuantity', 'autofocus'=>true])?>

<?php echo $this->Form->input('discount', ['placeholder' => 'giảm giá', 'class' =>'form-control','id' =>'inputDiscount', 'autofocus'=>true])?>
    <button type="submit" class="btn btn-default">Submit</button>
<?php  echo $this->Form->end();?>