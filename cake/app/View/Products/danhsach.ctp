

<div class="container">
    <table class="table" border='1' cellpadding='5'>
        <thead>
        <tr>
            <th>Product_id</th>
            <th>name</th>
            <th>slug</th>
            <th>description</th>
            <th>price</th>
            <th>content</th>
            <th>categories_id</th>
            <th>status_id</th>
            <th>create_time</th>
            <th>update_time</th>
            <th>image</th>
            <th>action</th>

        </tr>
        </thead>

        <tbody>
        <?php foreach ($data as $ix => $row): ?>
            <tr>
                <td><?php echo $row['Product']['id']; ?></td>
                <td><?php echo $row['Product']['name']; ?></td>
                <td><?php echo $row['Product']['slug']; ?></td>
                <td><?php echo $row['Product']['description']; ?></td>
                <td><?php echo $row['Product']['price']; ?></td>
                <td><?php echo $row['Product']['content']; ?></td>
                <td><?php echo $row['Product']['category_id']; ?></td>
                <td><?php echo $row['Product']['status_id']; ?></td>
                <td><?php echo $row['Product']['create_time']; ?></td>
                <td><?php echo $row['Product']['update_time']; ?></td>
                <td>
                    <img
                            src="<?php echo $this->webroot .'/'. 'files' . '/' . $row['Product']['category_id'] . '/' . 'img' . '/'
                                . $row['Product']['img'] ?>" alt="" width="200" height="auto">

                </td>

                <td>
                    <ul>
                        <li><?php echo $this->Html->link
                            ('Edit', array('action' => 'edit', $row['Product']['id'])); ?> </li>
                        <li><?php echo $this->Form->postLink
                            ('Delete', array('action' => 'delete', $row['Product']['id']
                            )); ?> </li>
                        <li><?php echo $this->Html->link
                            ('List', array('action' => 'index')); ?> </li>
                        <li><?php echo $this->Html->link
                            ('New', array('action' => 'add')); ?> </li>
                    </ul>
                </td>
            </tr>


        <?php endforeach ?>
        </tbody>
    </table>
</div>

<?php
echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled')); //Hiện thj nút Previous
echo " | ".$this->Paginator->numbers()." | "; //Hiển thi các số phân trang
echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled')); //Hiển thị nút next
echo " Page ".$this->Paginator->counter(); // Hiển thị tổng trang
?>
}
