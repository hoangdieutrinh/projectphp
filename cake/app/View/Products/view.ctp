<html>
<title></title>
<body>
<?php echo $this->Form->create('Product', array('url' => array('controller' => 'products', 'action' => 'search') )); ?>

<?php echo $this->Form->input('name', array('type' => 'text')); ?>
<?php echo $this->Form->submit('Search');
?>
<?php echo $this->Form->end();?>
<?php $this->Paginator->options(array('url' => $this->passedArgs)); ?>
<!--Hiển thị dữ liệu sau khi tìm kiếm-->
<div class="container">
    <table class="table" border='1' cellpadding='5'>
        <thead>
        <tr>
            <th>Product_id</th>
            <th>name</th>
            <th>description</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $ix => $row): ?>
        <tr>
            <td><?php echo $row['Product']['id']; ?></td>
            <td><?php echo $row['Product']['name']; ?></td>
            <td><?php echo $row['Product']['description']; ?></td>
            <?php endforeach ?>
        </tbody>
    </table>
</div>



        echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled')); //Shows the next and previous links

        echo " | " . $this->Paginator->numbers() . " | "; //Shows the page numbers

        echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled')); //Shows the next and previous links

        echo " Page " . $this->Paginator->counter(); // prints X of Y, where X is current page and Y is number of pages
    }
}
?>

