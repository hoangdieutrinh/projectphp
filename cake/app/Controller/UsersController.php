<?php
class UsersController extends AppController
{
    public $helpers = array('Html', 'Form', 'Session');
    public $components = array('Session');
    public function index()
    {
        $data = $this->User->find("all");
        $this->set("data", $data);
    }

   public function add()
   {
       if($this->request->is('post'))
       {
           $this->User->create();
           $userData = array(
               'role_id'=>$this->data['User']['role_id'],
               'username'=>$this->data['User']['username'],
               'password'=>$this->data['User']['password'],
               'email'=>$this->data['User']['email'],
               'address'=>$this->data['User']['address'],
               'gender'=>$this->data['User']['gender'],
               'dateofbirth'=>$this->data['User']['dateofbirth'],
               'img' => $this->data['User']['img']['name']
           );
           if ($this->User->save($userData)) {
               $path = WWW_ROOT . 'files' . DS . $this->request->data['User']['username'] . DS . 'avatar';
             /*  pr($path); exit();*/
               if (!is_dir($path)) {
                   mkdir($path, 0777, true);
               }
               $baseFileName = $this->request->data['User']['img']['name'];
               $tempFileName = $this->request->data['User']['img']['tmp_name'];
               if (!file_exists($path . $baseFileName)) {
                   move_uploaded_file($tempFileName, $path . DS . $baseFileName);
               }
               $this->Flash->success(__('Your post has been saved.'));
               return $this->redirect(array('action' => 'index'));
           }
           $this->Flash->error(__('Unable to add your post.'));
       }
       $roles = $this->User->Role->find('list', array('fields' => array('id', 'name')));
       $this->set(compact('roles'));
       }
    function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->User->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->User->id = $id;
            $userData = array(
                'role_id'=>$this->data['User']['role_id'],
                'username'=>$this->data['User']['username'],
                'password'=>$this->data['User']['password'],
                'email'=>$this->data['User']['email'],
                'address'=>$this->data['User']['address'],
                'gender'=>$this->data['User']['gender'],
                'dateofbirth'=>$this->data['User']['dateofbirth'],
                'img' => $this->data['User']['img']['name']
            );
            if ($this->User->save($userData)) {
                $this->Flash->success(__('Your product has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
        $roles = $this->User->Role->find('list', array('fields' => array('id', 'name')));
        $this->set(compact('roles'));
    }
    function delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->request->allowMethod(['post', 'delete']);

        if ($this->User->delete()) {
            $this->Flash->success(__('The post with id: {0} has been deleted.', h($id)));

        } else {
            $this->Flash->error(__('The product failed.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}