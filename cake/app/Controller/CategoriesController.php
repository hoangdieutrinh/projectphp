<?php

class CategoriesController extends AppController
{
    public function index()
    {
        $data=$this->Category->find("all");
        $this->set("data", $data);
    }
    public function add()
    {

        if ($this->request->is('post')) {
            $this->Category->create();
            $categoryData = array(
                'id' => $this->data['Category']['id'],
                'name' => $this->data['Category']['name'],
                'slug' => $this->data['Category']['slug'],
                'description' => $this->data['Category']['description'],
                'create_time'=> $this->data['Category']['create_time']
            );

            if ($this->Product->save( $categoryData)) {


                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your post.'));
        }


    }
    public function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Category->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid product'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Category->id = $id;
            if ($this->Category->save($this->request->data)) {
                $this->Flash->success(__('Your product has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }
        public function delete($id = null)
    {
        $this->Category->id = $id;
        if (!$this->Category>exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->request->allowMethod(['post', 'delete']);

        if ($this->Category->delete()) {
            $this->Flash->success(__('The post with id: {0} has been deleted.', h($id)));

        } else {
            $this->Flash->error(__('The product failed.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}