<?php

class ProductsController extends AppController
{

    var $helpers = array('Paginator','Html','Form');
    var $paginate = array();
    public function index()
    {
        $data = $this->Product->find("all");

        $this->set("data", $data);
    }

    public function add()
    {

        if ($this->request->is('post')) {
            $this->Product->create();
            $productData = array(
                'category_id' => $this->data['Product']['category_id'],
                'status_id' => $this->data['Product']['status_id'],
                'name' => $this->data['Product']['name'],
                'slug' => $this->data['Product']['slug'],
                'description' => $this->data['Product']['description'],
                'price' => $this->data['Product']['price'],
                'content' => $this->data['Product']['content'],
                'quantity' => $this->data['Product']['quantity'],
                'discount' => $this->data['Product']['discount'],
                'img' => $this->data['Product']['img']['name']
            );

            if ($this->Product->save($productData)) {
                $path = WWW_ROOT . 'files' . DS . $this->request->data['Product']['category_id'] . DS . 'img';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $baseFileName = $this->request->data['Product']['img']['name'];
                $tempFileName = $this->request->data['Product']['img']['tmp_name'];
                if (!file_exists($path . $baseFileName)) {
                    move_uploaded_file($tempFileName, $path . DS . $baseFileName);
                }

                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your post.'));
        }

        $categories = $this->Product->Category->find('list', array('fields' => array('id', 'name')));
        $statuses = $this->Product->Status->find('list', array('fields' => array('id', 'name')));
        $this->set(compact('categories', 'statuses'));

    }

    public function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Product->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid product'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Product->id = $id;
            if ($this->Product->save($this->request->data)) {
                $this->Flash->success(__('Your product has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
        $categories = $this->Product->Category->find('list', array('fields' => array('id', 'name')));
        $statuses = $this->Product->Status->find('list', array('fields' => array('id', 'name')));
        $this->set(compact('categories', 'statuses'));
    }

    public function delete($id = null)
    {
        $this->Product->id = $id;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->request->allowMethod(['post', 'delete']);

        if ($this->Product->delete()) {
            $this->Flash->success(__('The post with id: {0} has been deleted.', h($id)));

        } else {
            $this->Flash->error(__('The product failed.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    function danhsach()
    {
        $this->paginate=array(
            'limit'=>2,
            'order'=>array('id'=>'desc')
        );
        $data=$this->paginate("Product");
        $this->set("data",$data);
    }
    function view()
    {
        $conditions=array();
        $data= array();
        if(!empty($this->passedArgs)){
            if(isset($this->passedArgs ['Product.name']))//kiểm tra xem tồn tại name hay không?
            {
                $name=$this->passedArgs['Product.name'];
                $conditions[]=array("Product.name LIKE"=>"%$name%",);// điều kiện SQL
                $data['Product']['name']=$name; // cho giá trị nhập vào
            }

            $this->paginate=array('limit'=>2,'order'=>array('id'=>'desc'));
            $this->data=$data;// giữ lại data nhập vào sau khi submit
            $post=$this->paginate("Product",$conditions);

            $this->set("data",$post);
        }
    }
    function search()
    {
        $url['action']='view';
        foreach ($this->data as $key=>$value)
        {
            foreach ($value as $key2=>$value2)
            {
                $url[$key.'.'.$key2]=$value2;
            }
        }
        $this->redirect($url,null,true);// dùng để quay về action view
    }


}







