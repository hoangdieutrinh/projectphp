<?php
class NewsController extends AppController
{
    var $helpers = array('Paginator', 'Html', 'Form');
    var $paginate = array();

    public function index()
    {
        $data = $this->News->find("all");

        $this->set("data", $data); }

   public function add()    {
    if ($this->request->is('post'))
        {
                 $this->News->create();
                 $newsData = array(
                'user_id' => $this->data['News']['user_id'],
                'category_id' => $this->data['News']['category_id'],
                'slug' => $this->data['News']['slug'],
                'name' => $this->data['News']['name'],
                'description' => $this->data['News']['description'],
                'content' => $this->data['News']['content'],
                'img' => $this->data['News']['img']['name']
            );

            if ($this->News->save($newsData)) {
                $path = WWW_ROOT . 'files' . DS . $this->request->data['News']['name'] . DS . 'img';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                $baseFileName = $this->request->data['News']['img']['name'];
                $tempFileName = $this->request->data['News']['img']['tmp_name'];
                if (!file_exists($path . $baseFileName)) {
                    move_uploaded_file($tempFileName, $path . DS . $baseFileName);
                }

                $this->Flash->success(__('Your news has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your news.'));
        }

        $categories = $this->News->Category->find('list', array('fields' => array('id', 'name')));
       $users = $this->News->User->find('list', array('fields' => array('id', 'username')));
        $this->set(compact('categories', 'users'));

    }
    public function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->News->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid product'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->News->id = $id;
            if ($this->News->save($this->request->data)) {
                $this->Flash->success(__('Your News has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
        $categories = $this->News->Category->find('list', array('fields' => array('id', 'name')));
        $users = $this->News->User->find('list', array('fields' => array('id', 'username')));
        $this->set(compact('categories', 'users'));
    }

    public function delete($id = null)
    {
        $this->News->id = $id;
        if (!$this->News->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->request->allowMethod(['post', 'delete']);

        if ($this->News->delete()) {
            $this->Flash->success(__('The post with id: {0} has been deleted.', h($id)));

        } else {
            $this->Flash->error(__('The product failed.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}



