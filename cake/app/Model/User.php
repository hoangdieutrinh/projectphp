<?php
/**
 * Created by PhpStorm.
 * User: ST_Computer
 * Date: 4/17/2017
 * Time: 8:34 AM
 */
App::uses('Apmodel','Model');
class User extends AppModel
{
    public $belongsTo = array(
        'Role' => array(
            'className' => 'role',
            'foreignKey' => 'role_id'
        ));
}