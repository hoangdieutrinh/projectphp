<?php
/**
 * Created by PhpStorm.
 * User: ST_Computer
 * Date: 4/21/2017
 * Time: 10:30 AM
 */
App::uses('Apmodel','Model');
class Role extends AppModel
{
    public $hasMany= array(
        'User'=>array(
            'classname'=>'user',
            'foreignKey'=>'user_id',
        )
    );

}