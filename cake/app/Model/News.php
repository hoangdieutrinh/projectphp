<?php
App::uses('Apmodel','Model');
class News extends AppModel
{
    public $belongsTo = array(
        'User' => array(
            'className' => 'user',
            'foreignKey' => 'user_id'
        ),
        'Category'=>array(
            'classname'=>'category',
            'foreignKey' =>'category_id'

        ));

}